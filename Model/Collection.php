<?php
namespace Indaba\ZeroPrice\Model;

class Collection extends \Magento\CatalogSearch\Model\ResourceModel\Fulltext\Collection
{
    public function removeItemByKey($key) {
        parent::removeItemByKey($key);
        $this->_totalRecords--;
    }
}