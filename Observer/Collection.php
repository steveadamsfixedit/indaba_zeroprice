<?php

namespace Indaba\ZeroPrice\Observer;

class Collection implements \Magento\Framework\Event\ObserverInterface
{
    protected $price;
    
    public function __construct(
        \Anowave\Price\Model\Price $price
    ) {
        $this->price = $price;
    }

    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        $collection = $observer->getCollection();
        
        foreach ($collection as $key => $product) {
            if ($this->price->getPrice($product) == 0) {
                $collection->removeItemByKey($key);
                //$removed++;
            }
        }
    }
}