<?php

namespace Indaba\ZeroPrice\Observer;

class Product implements \Magento\Framework\Event\ObserverInterface
{
    protected $registry;
    
    public function __construct(
        \Magento\Framework\Registry $registry
    ) {
        $this->registry = $registry;
    }
    
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        $product = $observer->getProduct();
        
        if ($this->registry->registry('processed_zp' . $product->getId())) {
            return;
        } else {
            $this->registry->register('processed_zp' . $product->getId(), 'true');
        }
       
        if (
            $product->getFinalPrice() == 0
        ) {
            $product->setStatus(0);
        }
    }
}